import Common
import Data.Aeson (decodeStrict)
import Data.Maybe (fromJust, fromMaybe)
import JavaScript.Web.XMLHttpRequest
import Miso
import Miso.String (ms)
import MisoActionLogger

main :: IO ()
main = miso $ const App 
    { initialAction = FetchHeroes
    , model = initialModel
    , update = defaultActionLogger updateModel
    , view = homeRoute
    , events = defaultEvents
    , subs = []
    , mountPoint = Nothing
    }

updateModel :: Action -> Model -> Effect Action Model
updateModel NoOp m = noEff m
updateModel (SetHeroes heroes) m = noEff m { heroes_ = heroes }
updateModel FetchHeroes m = m <# do SetHeroes <$> xhrHeroes

xhrHeroes :: IO [Hero]
xhrHeroes = 
    fromMaybe [] . decodeStrict . fromJust . contents <$> xhrByteString req
    where req = Request GET uri Nothing [] False NoData
          uri = ms $ show linkHeroes

