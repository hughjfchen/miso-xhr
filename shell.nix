let

  pkgs = import ./nix/nixpkgs.nix ;

  server = pkgs.haskell.packages.ghc.callCabal2nix "miso-xhr" ./. {};
  client = pkgs.haskell.packages.ghcjs.callCabal2nix "miso-xhr" ./. {};

in

  {
    server = server.env;
    client = client.env;
  }

