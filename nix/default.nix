let

  pkgs = import ./nixpkgs.nix ;
  miso-xhr-src = ../. ;

  server = pkgs.haskell.packages.ghc.callCabal2nix "miso-xhr" ../. {};
  client = pkgs.haskell.packages.ghcjs.callCabal2nix "miso-xhr" ../. {};

in

  pkgs.runCommand "miso-xhr" { inherit client server; } ''
    mkdir -p $out/{bin,static}
    cp ${server}/bin/* $out/bin/

    #cp ${client}/bin/client.jsexe/all.js $out/static/all.js
    ${pkgs.closurecompiler}/bin/closure-compiler ${client}/bin/client.jsexe/all.js > $out/static/all.js

    cp ${miso-xhr-src}/static/*.png $out/static/
  ''

