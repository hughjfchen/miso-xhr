{ pkgs ? import <nixpkgs> {} }:

let

  miso-xhr = import ./default.nix;

  entrypoint = pkgs.writeScript "entrypoint.sh" ''
    #!${pkgs.stdenv.shell}
    $@
  '';

in

  pkgs.dockerTools.buildImage {
    name = "miso-xhr";
    tag = "latest";
    created = "now";
    config = {
      WorkingDir = "${miso-xhr}";
      Entrypoint = [ entrypoint ];
      Cmd = [ "${miso-xhr}/bin/server" ];
    };
  }

